﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : SinglestateUserOfStateMachine {

	[SerializeField] GameObject _enemyPrefab;
	int _enemyMaxCount = 10;
	float _xMax;

	//TODO в идеале надо сделать через пул объектов, что бы не использовать каждый раз долгие Instantiate и Destroy

	protected override void OnGoingIntoState() {
		StartCoroutine(EnemyGenerator());
	}

	protected override void OnPassingFromState() {
		StopCoroutine(EnemyGenerator());
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}

	protected override void OnStart() {
		_xMax = (float) Screen.width / Screen.height * Camera.main.GetComponent<Camera>().orthographicSize;
		StartCoroutine(EnemyGenerator());
	}

	IEnumerator EnemyGenerator() {
		WaitForSeconds waitForSeconds = new WaitForSeconds(0.1f);
		while (true) {
			if (transform.childCount < _enemyMaxCount) {
				Vector3 spawnPosition = new Vector3(Random.Range(-_xMax,_xMax), 0, transform.position.z);
				Instantiate(_enemyPrefab, spawnPosition, transform.rotation, transform);
			}
			yield return waitForSeconds;
		}
	}
}
