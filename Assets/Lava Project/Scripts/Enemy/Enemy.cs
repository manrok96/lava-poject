﻿using System.Collections;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class Enemy : SinglestateUserOfStateMachine {

	[SerializeField] TextMeshPro _enemyHpText;
	public int CurrentHp { get; private set; }

	Vector3 _startPos;
	Vector3 _endPos;
	Vector3[] _wayPoints;

	Material _myMeshRenderer;
	Color _defaultColor;


	protected override void OnGoingIntoState() {
		transform.DOPlay();
	}

	protected override void OnPassingFromState() {
		transform.DOPause();
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}

	protected override void OnStart() {
		CurrentHp = Random.Range(2, GameManager.Instance.EnemyMaxHp + 1);
		_enemyHpText.text = CurrentHp.ToString();
		
		_startPos = transform.position;
		_endPos = new Vector3(_startPos.x, _startPos.y, -9.5f);
		_wayPoints = new [] {_endPos, _startPos};
		float randomDuration = Random.Range(2.9f, 5f);
		
		transform.DOPath(_wayPoints, randomDuration).SetLoops(-1, LoopType.Yoyo);

		_myMeshRenderer = GetComponent<MeshRenderer>().material;
		_defaultColor = _myMeshRenderer.color;
	}	

	void OnTriggerEnter(Collider other) {
		if (other.CompareTag("Shot")) {
			Debug.Log("Enemy shot");
			
			CurrentHp--;
			
			if (CurrentHp > 0) {
				_enemyHpText.text = CurrentHp.ToString();
				StartCoroutine(BlinkColor());
			}
			else {
				Destroy(gameObject);  //TODO возвращать в пулл
			}
		}
	}

	IEnumerator BlinkColor() {
		WaitForSeconds waitForSeconds = new WaitForSeconds(GameManager.Instance.PlayerFireRate / 2);

		_myMeshRenderer.DOColor(Color.blue, 0.1f);
		yield return waitForSeconds;
		_myMeshRenderer.DOColor(_defaultColor, 0.1f);
	}
}
