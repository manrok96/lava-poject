﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUserOfStateMachine : MonoBehaviour {

	protected abstract void Start();

	//сработает при переходе на другую сцену
	protected virtual void OnDisable() {
		ApplicationStateManager.Instance.StateAppChanged -= SetActive;
	}

	/// <summary>
	///   <para>Вызывается при вызове Start() в базовом классе.</para>
	///   <para>Нужно, что бы не писать каждый раз base.Start() в дочерних классах при переопределении Start(), т.к. переопределять базовный старт точно не понадобится, но можно забыть его вызвать, из за чего объект не подпишется на событие смены состояния.</para>
	/// </summary>
	protected abstract void OnStart();

	protected abstract void SetActive(ApplicationStateManager.StateApp previousState, ApplicationStateManager.StateApp newSate);

	protected virtual void OnGoingIntoState(){ }
	
	protected virtual void OnPassingFromState(){ }
}
