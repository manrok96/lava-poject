﻿using UnityEngine;
using System.Collections;

public class Shot : SinglestateUserOfStateMachine {
	
	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}

	protected override void OnStart() {
		GetComponent<Rigidbody>().velocity = transform.forward * GameManager.Instance.BulletSpeed;
	}

	void OnTriggerEnter(Collider other) {
		Destroy(gameObject);
	}
}
