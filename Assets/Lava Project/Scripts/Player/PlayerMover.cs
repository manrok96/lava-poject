﻿using UnityEngine;

public class PlayerMover : SinglestateUserOfStateMachine {

	readonly float _screenWidth = Screen.width;

	float _xMax;
	float _lastTouchPositionX;
	float _myNewPositionX;

	#region UserOfStateMachine

	protected override void OnGoingIntoState() {
		InputTouchHandler.Instance.OnOneTouch += MovePlayer;
	}

	protected override void OnPassingFromState() {
		InputTouchHandler.Instance.OnOneTouch -= MovePlayer;
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}

	#endregion

//	protected override void OnDisable() {
//		base.OnDisable();
//		InputTouchHandler.Instance.OnOneTouch -= MovePlayer;
//	}
	
	protected override void OnStart() {
		_xMax = _screenWidth / Screen.height * Camera.main.GetComponent<Camera>().orthographicSize -
		        transform.localScale.x / 2;
	}


	void MovePlayer(Touch touch) {
		switch (touch.phase) {
			case TouchPhase.Began:
				_lastTouchPositionX = touch.position.x;
				_myNewPositionX = -_xMax + (_xMax + _xMax) * _lastTouchPositionX / _screenWidth;
				break;
			case TouchPhase.Moved:
				_lastTouchPositionX = touch.position.x;
				_myNewPositionX = -_xMax + (_xMax + _xMax) * _lastTouchPositionX / _screenWidth;
				transform.position = Vector3.Lerp(transform.position,
					new Vector3(_myNewPositionX, transform.position.y, transform.position.z), GameManager.Instance.PlayerSpeed);
				break;
			case TouchPhase.Stationary:
				transform.position = Vector3.Lerp(transform.position,
					new Vector3(_myNewPositionX, transform.position.y, transform.position.z), GameManager.Instance.PlayerSpeed * 0.75f);
				break;
		}
	}

#if UNITY_EDITOR
	void Update() {
		if (Input.anyKey) {
			if (Input.GetKey(KeyCode.A)) {
				transform.position = Vector3.Lerp(transform.position,
					new Vector3(-_xMax, transform.position.y, transform.position.z), 0.01f);
			}
			else if (Input.GetKey(KeyCode.D)) {
				transform.position = Vector3.Lerp(transform.position,
					new Vector3(_xMax, transform.position.y, transform.position.z), 0.01f);
			}

		}
	}
#endif
	public void OnStateAppChanged(ApplicationStateManager.StateApp prevstate, ApplicationStateManager.StateApp newstate) {
		Debug.Log("KEK");
	}
}
