﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHpController : SinglestateUserOfStateMachine {

	[SerializeField] TextMeshProUGUI _hpText;
	int _playerCurrentHp;


	protected override void OnStart() {
		_playerCurrentHp = GameManager.Instance.PlayerMaxHp;
		SetTextHp();
		
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}
	
	protected override void OnGoingIntoState() {
		_playerCurrentHp = GameManager.Instance.PlayerMaxHp;
		SetTextHp();
	}

	void OnTriggerEnter(Collider other) {
		if (!other.CompareTag("Enemy")) 
			return;
		
		_playerCurrentHp--;
		SetTextHp();
		if (_playerCurrentHp <= 0) {
			ApplicationStateManager.Instance.SetState((int)ApplicationStateManager.StateApp.GameOver);
		}
	}

	void SetTextHp() {
		_hpText.text = _playerCurrentHp.ToString();	
	}
}
