﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoter : SinglestateUserOfStateMachine {

	[SerializeField] Transform _shotSpawn;
	[SerializeField] GameObject _shot;



	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Play;
	}

	protected override void OnGoingIntoState() {
		StartCoroutine(Shooter());
	}

	protected override void OnPassingFromState() {
		StopCoroutine(Shooter());
	}

	IEnumerator Shooter() {
		WaitForSeconds waitForSeconds = new WaitForSeconds(GameManager.Instance.PlayerFireRate);
		
		while (GetMainState() == ApplicationStateManager.Instance.CurrentlyApplicationState) {
			Instantiate(_shot, _shotSpawn.position, _shotSpawn.rotation);
			yield return waitForSeconds;
		}
	}
}
