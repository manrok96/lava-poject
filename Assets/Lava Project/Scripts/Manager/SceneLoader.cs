﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public sealed class SceneLoader : Singleton<SceneLoader> {
	
	[SerializeField] string _autostartSceneName;
	float _delay = 1.5f;
	
	public event Action<float> OnProgressUpdated;

	public float Delay {
		get { return _delay; }
	}


	void Start() {
//		if (_autostartSceneName != "") {
//			try {
//				LoadScene(_autostartSceneName, LoadSceneMode.Single);
//			}
//			catch (Exception e) {
//				Debug.Log(e.Message);
//			}
//		}
		DontDestroyOnLoad(this);
	}

	public void LoadScene(int sceneNumber, ApplicationStateManager.StateApp transState, LoadSceneMode mode = LoadSceneMode.Single) {
		StartCoroutine(AsyncSceneLoad(sceneNumber, transState,mode));
	}

	public void LoadScene(string sceneName, ApplicationStateManager.StateApp transState) {
		try {
			StartCoroutine(AsyncSceneLoad(sceneName, transState, LoadSceneMode.Single));
		}
		catch (Exception e) {
			Debug.LogError(e.Message);
		}
	}

	public void LoadScene(string sceneName, ApplicationStateManager.StateApp transState, LoadSceneMode mode) {
		try {
			StartCoroutine(AsyncSceneLoad(sceneName, transState, mode));
		}
		catch (Exception e) {
			Debug.LogError(e.Message);
		}
	}

	IEnumerator AsyncSceneLoad(int sceneNumber, ApplicationStateManager.StateApp transState, LoadSceneMode mode = LoadSceneMode.Single) {
		
		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneNumber, mode);

		while (!asyncOperation.isDone) {
			yield return null;
		}
		ApplicationStateManager.Instance.SetState((int) transState);
	}

	IEnumerator AsyncSceneLoad(string sceneName, ApplicationStateManager.StateApp transState, LoadSceneMode mode = LoadSceneMode.Single) {		
		yield return new WaitForSeconds(Delay);

		AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(sceneName, mode);

		while (!asyncOperation.isDone) {
			if (OnProgressUpdated != null)
				OnProgressUpdated(asyncOperation.progress);
			yield return null;
		}

//		asyncOperation = SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());

		while (!asyncOperation.isDone) {
			yield return null;
		}

		ApplicationStateManager.Instance.SetState((int) transState);
	}
}
