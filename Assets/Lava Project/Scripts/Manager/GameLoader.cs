﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour {

	[SerializeField] GameObject _gameManagerPrefab;
	[SerializeField] GameObject _appStateManagerPrefab;
	[SerializeField] GameObject _touchInspector;
	[SerializeField] GameObject _sceneLoader;
	
	void Awake() {
		if (GameManager.Instance == null) {
			Instantiate(_gameManagerPrefab);
		}

		if (ApplicationStateManager.Instance == null) {
			Instantiate(_appStateManagerPrefab);
		}

		if (InputTouchHandler.Instance == null) {
			Instantiate(_touchInspector);
		}

		if (SceneLoader.Instance == null) {
			Instantiate(_sceneLoader);
		}
	}
}
