﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;


public sealed class ApplicationStateManager : Singleton<ApplicationStateManager> {
	public enum StateApp {
		Menu = 0,
		Settings = 1,
		Play = 2,
		GameOver = 3,
		Transition = 4
	}

	public delegate void StateChange(StateApp prevState, StateApp newState);

	public event StateChange StateAppChanged;

	public StateApp LastState { get; private set; }

	StateApp _currentlyApplicationState = StateApp.Menu;

	public StateApp CurrentlyApplicationState {
		get { return _currentlyApplicationState; }
		private set {
			LastState = _currentlyApplicationState;
			_currentlyApplicationState = value;
			OnStateAppChanged(LastState, _currentlyApplicationState);
		}
	}

	void Start() {
		DontDestroyOnLoad(gameObject);
		LastState = StateApp.Menu;
	}

	public void SetState(int state) {
		if (StateExist(state))
			CurrentlyApplicationState = (StateApp) state;
		else
			throw new Exception("Неизвестное состояние");
	}

	static bool StateExist(int state) {
		string nameState = ((StateApp) state).ToString();
		return Enum.GetNames(typeof(StateApp)).Contains(nameState);
	}

	void OnStateAppChanged(StateApp previousState, StateApp newsSate) {
		Debug.Log("previousState: " + previousState + "; newsSate: " + newsSate);
		StateAppChanged?.Invoke(previousState, newsSate);
	}
}