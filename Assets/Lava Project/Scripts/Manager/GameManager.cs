﻿using UnityEngine;
using UnityEngine.Events;


public class GameManager : Singleton<GameManager> {

	public float PlayerSpeed = 0.4f;
	
	public float PlayerFireRate = 0.25f;

	public int PlayerMaxHp = 3;

	public int EnemyMaxHp = 10;

	public float BulletSpeed = 20;

	
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
}
