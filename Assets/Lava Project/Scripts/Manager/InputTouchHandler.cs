﻿using UnityEngine;


public sealed class InputTouchHandler : Singleton<InputTouchHandler> {
	
	public delegate void OneTouch(Touch t1);
	public event OneTouch OnOneTouch;

	public delegate void TwoTouch(Touch t1, Touch t2);
	public event TwoTouch OnTwoTouch;

	public delegate void ThreeTouch(Touch t1, Touch t2, Touch t3);
	public event ThreeTouch OnThreeTouch;

	void Start() {
		DontDestroyOnLoad(this);
	}

	void Update() {
		if (ApplicationStateManager.Instance.CurrentlyApplicationState != ApplicationStateManager.StateApp.Play) {
			return;
		}
		if (Input.touchCount == 1 && OnOneTouch != null) {
			OnOneTouch(Input.GetTouch(0));
		}
		else if (Input.touchCount == 2 && OnTwoTouch != null) {
			OnTwoTouch(Input.GetTouch(0), Input.GetTouch(1));
		}
		else if (Input.touchCount == 3 && OnThreeTouch != null) {
			OnThreeTouch(Input.GetTouch(0), Input.GetTouch(1), Input.GetTouch(2));
		}
	}
}