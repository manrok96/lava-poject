﻿

public abstract class SinglestateUserOfStateMachine : BaseUserOfStateMachine {
	
	protected sealed override void Start() {
		ApplicationStateManager.Instance.StateAppChanged += SetActive;
		OnStart();
	}

	protected sealed override void SetActive(ApplicationStateManager.StateApp previousState, ApplicationStateManager.StateApp newSate) {
		ApplicationStateManager.StateApp mainState = GetMainState();
		if (newSate == mainState) {
			OnGoingIntoState();
		}
		else if (previousState == mainState) {
			OnPassingFromState();
		}
	}

	protected override void OnStart() { }

	/// <summary>
	///   <para>При каком состоянии нужно вызвать OnPassingFromState()</para>
	///   <para>return ApplicationStateManager.StateApp.StateName</para>
	/// </summary>
	protected abstract ApplicationStateManager.StateApp GetMainState();
}
