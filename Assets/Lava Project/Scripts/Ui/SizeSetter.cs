﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SizeSetter : MonoBehaviour {
	void Start () {
		GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
		GetComponent<RectTransform>().anchoredPosition = new Vector2(-Screen.width,0);
	}
}
