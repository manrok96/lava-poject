﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameOver : SinglestateUserOfStateMachine {

	protected override void OnGoingIntoState() {
		transform.DOMoveX((float)Screen.width/2, 1);
	}

	protected override void OnPassingFromState() {
		transform.DOMoveX(-Screen.width, 1);
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.GameOver;
	}

	protected override void OnStart() {
		transform.DOMoveX(-Screen.width, 0);
	}

}
