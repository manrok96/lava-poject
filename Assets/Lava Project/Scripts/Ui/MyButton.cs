﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class MyButton : SinglestateUserOfStateMachine {


	[Header("При нажатии сменить состояние на:")]
	[SerializeField] ApplicationStateManager.StateApp _transitionApplicationState;
	
	[Header("Оставить пустым, если не нужно грузить сцену")]
	[SerializeField] string _sceneName = "";

	Button _myButton;

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Transition;
	}

	protected override void OnGoingIntoState() {
		_myButton.interactable = false;
	}

	protected override void OnPassingFromState() {
		StartCoroutine(InteractableButton(0.6f));
	}

	protected override void OnStart() {
		_myButton = GetComponent<Button>();
		
		_myButton.onClick.AddListener(OnClick);	
	}

	void OnClick() {
		if (_sceneName != "") {
			ApplicationStateManager.Instance.SetState((int)ApplicationStateManager.StateApp.Transition);
			SceneLoader.Instance.LoadScene(_sceneName, _transitionApplicationState);
		}
		else {
			ApplicationStateManager.Instance.SetState((int)ApplicationStateManager.StateApp.Transition);

			ApplicationStateManager.Instance.SetState((int)_transitionApplicationState);
		}
	}

	IEnumerator InteractableButton(float delay) {
		yield return new WaitForSeconds(delay);
		_myButton.interactable = true;
	}
}
