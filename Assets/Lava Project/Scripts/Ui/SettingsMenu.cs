﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.UI;

public class SettingsMenu : SinglestateUserOfStateMachine {

	//[SerializeField] InputField _playerSpeed;
	[SerializeField] Slider _speedSlider;
	[SerializeField] Text _speedText;
	
	[SerializeField] Slider _fireRateSlider;
	[SerializeField] Text _fireRateText;

	protected override void OnStart() {
		_speedSlider.onValueChanged.AddListener(OnSpeedSliderValueChange);
		_speedSlider.value = GameManager.Instance.PlayerSpeed;
		_speedText.text = _speedSlider.value.ToString();
		
		_fireRateSlider.onValueChanged.AddListener(OnFireRateSliderValueChange);
		_fireRateSlider.value = GameManager.Instance.PlayerFireRate;
		_fireRateText.text = _fireRateSlider.value.ToString();
	}
	
	void OnSpeedSliderValueChange(float value) {
		GameManager.Instance.PlayerSpeed = value;
		_speedText.DOText("" + value, 0.5f);
	}

	void OnFireRateSliderValueChange(float value) {
		GameManager.Instance.PlayerFireRate = value;
		_fireRateText.DOText("" + value, 0.5f);
	}

	protected override void OnGoingIntoState() {
		transform.DOMoveX(Screen.width/2, 1);
	}

	protected override void OnPassingFromState() {
		transform.DOMoveX(-Screen.width, 1);
	}

	protected override ApplicationStateManager.StateApp GetMainState() {
		return ApplicationStateManager.StateApp.Settings;
	}

	
}
