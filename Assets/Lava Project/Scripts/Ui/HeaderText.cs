﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class HeaderText : MultistateUserOfStateMachine {
	
	[SerializeField] Text _headerText;

	protected override void OnGoingIntoState() {
		switch (ApplicationStateManager.Instance.CurrentlyApplicationState) {
			case ApplicationStateManager.StateApp.Menu:
				_headerText.DOText("Главное меню", 0.6f);
				break;
			case ApplicationStateManager.StateApp.Settings:
				_headerText.DOText("Настройки", 1);
				break;
		}
	}
}
