﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScaleSetter : MonoBehaviour {

	public void SetScale(float scaleFactor)
	{
		transform.localScale = new Vector3(scaleFactor, scaleFactor, scaleFactor);
	}
}
