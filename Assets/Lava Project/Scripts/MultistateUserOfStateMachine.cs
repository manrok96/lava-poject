﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class MultistateUserOfStateMachine : BaseUserOfStateMachine {

	[Header("В каких состояниях объект активен: ")] [SerializeField]
	protected List<ApplicationStateManager.StateApp> TargetStates;


	protected sealed override void Start() {
		if (TargetStates.Count > 0) {
			ApplicationStateManager.Instance.StateAppChanged += SetActive;
			OnStart();
		}
		else {
			throw new Exception("У объекта " + gameObject.name + " не перечислены состояния при которых он активен");
		}	
	}

	protected sealed override void SetActive(ApplicationStateManager.StateApp previousState, ApplicationStateManager.StateApp newSate) {
		if (TargetStates.Contains(newSate)) {
			OnGoingIntoState();
		}
		else {
			OnPassingFromState();
		}
	}

	protected override void OnStart() { }
}
