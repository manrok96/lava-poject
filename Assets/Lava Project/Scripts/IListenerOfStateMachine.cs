﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IListenerOfStateMachine {
	
	void OnStateAppChanged(ApplicationStateManager.StateApp previousState, ApplicationStateManager.StateApp newState);

}
                                                                                                                 